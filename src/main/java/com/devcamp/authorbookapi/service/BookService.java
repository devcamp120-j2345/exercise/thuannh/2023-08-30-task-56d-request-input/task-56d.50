package com.devcamp.authorbookapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.model.Book;

@Service
public class BookService {
    @Autowired
    private AuthorService authorService;

    Book book1 = new Book("Book 1", 100000, 3);
    Book book2 = new Book("Book 2", 200000, 5);
    Book book3 = new Book("Book 3", 300000, 7);
    Book book4 = new Book("Book 4", 150000, 2);
    Book book5 = new Book("Book 5", 250000, 4);
    Book book6 = new Book("Book 6", 350000, 6);
    

    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBook = new ArrayList<>();

        book1.setAuthors(authorService.getAuthorBook1());
        book2.setAuthors(authorService.getAuthorBook2());
        book3.setAuthors(authorService.getAuthorBook3());
        book4.setAuthors(authorService.getAuthorBook4());
        book5.setAuthors(authorService.getAuthorBook5());
        book6.setAuthors(authorService.getAuthorBook6());

        allBook.add(book1);
        allBook.add(book2);
        allBook.add(book3);
        allBook.add(book4);
        allBook.add(book5);
        allBook.add(book6);

        return allBook;
    }

    public Book getBookAtIndex(int index) {

        ArrayList<Book> listBook = getAllBooks();

        if (index < 0 || index >= listBook.size()) {
            return null;
        }
        return listBook.get(index);
    }

}
