package com.devcamp.authorbookapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.model.Author;

@Service
public class AuthorService {
    Author anhlt = new Author("Tuấn Anh", "anhlt@gmail.com", 'M');
    Author huypt = new Author("Tuấn Huy", "huypt@gmail.com", 'M');
    Author hoangdt = new Author("Tiến Hoàng", "hoangdt@gmail.com", 'M');
    Author ngaht = new Author("Thúy Nga", "ngaht@gmail.com", 'F');
    Author vynt = new Author("Thúy Vy", "vynt@gmail.com", 'F');
    Author thuannh = new Author("Hữu Thuận", "thuannh@gmail.com", 'M');

    Author toannd = new Author("Đắc Toàn", "toannd@gmail.com", 'M');
    Author lanlh = new Author("Hoàng Lan", "lanlh@gmail.com", 'F');
    Author manhnd = new Author("Đức Mạnh", "manhnd@gmail.com", 'M');
    Author ngoctd = new Author("Diệp Ngọc", "ngoctd@gmail.com", 'F');
    Author dieptn = new Author("Ngọc Diệp", "dieptn@gmail.com", 'F');
    Author khant = new Author("Trọng Kha", "khant@gmail.com", 'M');

    public ArrayList<Author> getAuthorBook1() {
        ArrayList<Author> authorBook1 = new ArrayList<>();

        authorBook1.add(anhlt);
        authorBook1.add(thuannh);

        return authorBook1;
    }

    public ArrayList<Author> getAuthorBook2() {
        ArrayList<Author> authorBook2 = new ArrayList<>();

        authorBook2.add(huypt);
        authorBook2.add(vynt);

        return authorBook2;
    }

    public ArrayList<Author> getAuthorBook3() {
        ArrayList<Author> authorBook3 = new ArrayList<>();

        authorBook3.add(hoangdt);
        authorBook3.add(ngaht);

        return authorBook3;
    }

    public ArrayList<Author> getAuthorBook4() {
        ArrayList<Author> authorBook4 = new ArrayList<>();

        authorBook4.add(toannd);
        authorBook4.add(lanlh);

        return authorBook4;
    }

    public ArrayList<Author> getAuthorBook5() {
        ArrayList<Author> authorBook5 = new ArrayList<>();

        authorBook5.add(manhnd);
        authorBook5.add(ngoctd);

        return authorBook5;
    }

    public ArrayList<Author> getAuthorBook6() {
        ArrayList<Author> authorBook6 = new ArrayList<>();

        authorBook6.add(dieptn);
        authorBook6.add(khant);

        return authorBook6;
    }

    public ArrayList<Author> getAllAuthors() {
        ArrayList<Author> allAuthor = new ArrayList<>();

        allAuthor.add(anhlt);
        allAuthor.add(huypt);
        allAuthor.add(hoangdt);
        allAuthor.add(ngaht);
        allAuthor.add(vynt);
        allAuthor.add(thuannh);

        return allAuthor;
    }

    public Author filterAuthorsByEmail(String authorEmail) {
        ArrayList<Author> authors = new ArrayList<>();

        authors.add(anhlt);
        authors.add(huypt);
        authors.add(hoangdt);
        authors.add(ngaht);
        authors.add(vynt);
        authors.add(thuannh);

        Author findAuthor = new Author();

        for (Author authorElement : authors) {
            if (authorElement.getEmail().equals(authorEmail)) {
                findAuthor = authorElement;
            }
        }
        return findAuthor;
    }

    public ArrayList<Author> filterAuthorsByGender(char authorGender) {
        ArrayList<Author> authors = new ArrayList<>();

        authors.add(anhlt);
        authors.add(huypt);
        authors.add(hoangdt);
        authors.add(ngaht);
        authors.add(vynt);
        authors.add(thuannh);

        authors.add(toannd);
        authors.add(lanlh);
        authors.add(manhnd);
        authors.add(ngoctd);
        authors.add(dieptn);
        authors.add(khant);

        ArrayList<Author> findAuthor = new ArrayList<>();

        for (Author authorElement : authors) {
            if (authorElement.getGender() == authorGender) {
                findAuthor.add(authorElement);
            }
        }

        return new ArrayList<>(findAuthor);
    }
}
