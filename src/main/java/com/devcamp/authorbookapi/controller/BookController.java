package com.devcamp.authorbookapi.controller;



import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.authorbookapi.model.Book;
import com.devcamp.authorbookapi.service.BookService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBook = bookService.getAllBooks();

        return allBook;
    }

    @GetMapping("/books/{bookId}")
    public Book getBookById(@PathVariable int bookId) {
        return bookService.getBookAtIndex(bookId);
    }
}
